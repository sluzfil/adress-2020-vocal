# ADReSS Challenge AD inference with minimal speech/silence feature set


## Goals

To investigate the accuracy of a minimalistic on-off speech patterns for AD prediction.

To classify the ADReSS audio dataset (cookie data) into two categories: AD
and normal. 

## Abstract Data Representation

Each data point consists of a few simple 'functionals' derived from the
speakers vocalisation signal. So, a data instance can be described as
the following undecuples (3 alternative data representations):

1. Speech only:
```
    s = (age, gender, mean(d), sd(d), median(d), mean(r), max(r),        (1)
         min(r), sd(r), count(u), ad)
```
2. Speech and silence (minimal):
```
    s = (age, gender, mean(d), sd(d), median(d),                         (2)
         mean(r), max(r), min(r), sd(r),
         mean(s), sd(s), median(s),
         count(u), ad)
```
3. Speech and silence (min/max):
```
    s = (age, gender, mean(d), sd(d), median(d), max(d), min(d),         (3)
         mean(r), max(r), min(r), sd(r),
         mean(s), sd(s), median(s),  max(s), min(s),
         count(u), ad)
```

where d is the duration of utterances, r is the speaking rate, s is
the duration of silences count(u) gives the number of utterances, and
ad is TRUE for AD diagnosis and FALSE for controls.

## Software:

The software used was:

- the R environment 
  R version 3.4.4 (2018-03-15) -- "Someone to Lean On"
  Platform: x86_64-pc-linux-gnu (64-bit)
  (http://cran.r-project.org)

- the vocaldia package, v0.8.3:
  https://cran.r-project.org/package=vocaldia

- the rJava package v0.9-12  (for WEKA integration): 
  http://cran.r-project.org/web/packages/rJava/
 
- the RWeka package v0.4-42 (a wrapper for the WEKA API):
  http://cran.r-project.org/web/packages/RWeka/
       
- R/ADReSS.R:  containing the functions
  needed to run the experiment, including:

  - arff file generation
  - computation evaluation metrics (P, R, A, F_n)
  - batch execution of experiments

- ../../util/silencedetection.pl:
  My quick-and-dirty script for detecting silences within the
  utterances (as unfortunately the Pitt data do not annotate silences
  within utterances, so we need to detect those silences by other
  means). It uses ffmpeg (https://ffmpeg.org/) for processing the MP3 files.

- The espeak synthesizer for our speech-rate estimation 'hack'
  (i.e. estimate a 'speaking ratio' as the ratio of actual the
  duration of an utterance to the duration of an utterance synthesized
  by espeak at 160 WPM):
  http://espeak.sourceforge.net/

- Praat, for computation of syllable-nuclei based speaking rate
  estimation and silence detection, and the Praat scipt that automates
  this (../../util/praat-script-syllable-nuclei-v2)

- Optionally, audacity (http://www.audacityteam.org/) to visualise the
  speech files and labels.

## Data preparation:

1. First we generate the silence profiles for all files:

```shell
for f in `ls ~/tmp/ADReSS-IS2020-data/train/Full_wave_enhanced_audio/cc/*.wav` ; do perl silencedetection.pl -f  $f > ~/lib/projects/DTP/HRI-Dementia/data/ADReSS/silence/`basename $f`-sil.csv ; done

for f in `ls ~/tmp/ADReSS-IS2020-data/train/Full_wave_enhanced_audio/cd/*.wav` ; do perl silencedetection.pl -f  $f > ~/lib/projects/DTP/HRI-Dementia/data/ADReSS/silence/`basename $f`-sil.csv ; done

for f in `ls ~/tmp/ADReSS-IS2020-data/test/Full_wave_enhanced_audio/*.wav` ; do perl silencedetection.pl -f  $f > ~/lib/projects/DTP/HRI-Dementia/data/ADReSS/silence/`basename $f`-sil.csv ; done
```
2. We now create the speech rate profiles for each utterance on each .cha
file (replacing '~/tmp/ADReSS-IS2020-data/' by the location of the
dataset on your machine):

```r
source('R/audioproc.R')

makeSpeechRatesForDirectory(dir=c('~/tmp/ADReSS-IS2020-data/train/transcription/cc/', '~/tmp/ADReSS-IS2020-data/train/transcription/cd/', '~/tmp/ADReSS-IS2020-data/test/transcription/'), srdir='data/speech_rate/')
```

3, Then we load the datasets using vocaldia, as follows: 

```r
source('R/ADReSS.R')
library(vocaldia)

a2 <- loadADReSSdata()
```

If you would like to buid this from scratch rather than use
loadADReSSdata(), this is what it does:

  a. Extrat the features from the individual ADReSS files:

```r
x <- makeVocalStatsDataset(dir=c('~/tmp/ADReSS-IS2020-data/train/transcription/cc', '~/tmp/ADReSS-IS2020-data/train/transcription/cd'), sildir='data/silence', silsuffix='.wav-sil.csv', srdir='data/ADReSS/speech_rate')
```

  b. Then load the labels:

```r
z <- read.csv('../data/ADReSS/meta_data_test.txt', sep=';', strip.white=T)
```

  c. And now replace the values in y:

```r
levels(y$dx) <- c('Control', 'ProbableAD')
y$dx[z$Label==1] <- "ProbableAD"
y$mmse <- z$mmse
```

## Classification Results

### LOSOCV on training set:

The training and test set can be created using the following
functions (for feature set (1), for instane; see above):

```r
train <- getADReSsubSet(a2$train, features=1)
test <- getADReSsubSet(a2$test, features=1)
```

Install some classifier packages using WPM, if necessary:

```r
## for LDA
WPM('install-package', 'discriminantAnalysis')
## for SVM
WPM('install-package', 'LibLINEAR')
## See some info
WPM('package-info', 'installed', 'discriminantAnalysis')
WPM('package-info', 'installed', 'LibLINEAR'')
```

And then tested on, say, an LDA classifier:

```r
    r <- runExperiment(train, nfold = -1, 
            classifier =
                make_Weka_classifier("weka/classifiers/functions/LDA"),
            target = "ad", 
            controlpars = alist(`do-not-check-capabilities` = TRUE))
```

Use this to print a summary of results:

```r
    printResultsSummary(r,1)

Summary for experiment "Prediction Experiment", Avg over 108-fold evaluation for 
runExperiment(train, nfold = -1, classifier = make_Weka_classifier("weka/classifiers/functions/LDA"),     target = "ad", controlpars = alist(`do-not-check-capabilities` = TRUE))
on classifier classifier and threshold 0.
=====================================

TRUE                     FALSE
-------------------      ----------------------
Accuracy:   0.389         Accuracy:  0.630
Precision:  0.512         Precision: 0.507 
Recall:     0.389         Recall:    0.630 
F_1:        0.442         F_1:       0.562
Precision:  0.389         Precision: 0.630       (macro-averaged)
Recall:     0.389         Recall:    0.630       (macro-averaged)
F_1:        0.389         F_1:       0.630       (macro-averaged)
Overall accuracy: 0.509
```

For LOSOCV and testing on all classifiers used in
https://arxiv.org/abs/2004.06833

```r
    ## create train/test sets for feature set (2)
    train <- getADReSsubSet(a2$train, features=2)
    test <- getADReSsubSet(a2$test, features=2)
    rs2 <- runADReSSclassificationBatch(train,test)
    
    ## CV debug info omitted
    Testing
========
data frame passed as argument (Target: ad; Generality: 50%)

Experiment  Prediction Experiment 
Evaluating on classifier runExperiment(train, test = test, classifier = make_Weka_classifier("weka/classifiers/functions/LDA"), target = "ad", controlpars = alist(`do-not-check-capabilities` = TRUE)) 
=========================================================================

Evaluating on classifier classifier - threshold: 0 
=======================================
Split: 0%  -- Test set given separately

TRUE                     FALSE
-------------------      ----------------------
Accuracy:   0.625         Accuracy:  0.583
Precision:  0.600         Precision: 0.609      
Recall:     0.625         Recall:    0.583      
F_1:        0.612         F_1:       0.596
Overall accuracy: 0.604
data frame passed as argument (Target: ad; Generality: 50%)

Experiment  Prediction Experiment 
Evaluating on classifier runExperiment(train, test = test, classifier = make_Weka_classifier("weka/classifiers/trees/J48"), target = "ad", controlpars = alist(M = 20)) 
=========================================================================

Evaluating on classifier classifier - threshold: 0 
=======================================
Split: 0%  -- Test set given separately

TRUE                     FALSE
-------------------      ----------------------
Accuracy:   0.500         Accuracy:  0.625
Precision:  0.571         Precision: 0.556      
Recall:     0.500         Recall:    0.625      
F_1:        0.533         F_1:       0.588
Overall accuracy: 0.562
data frame passed as argument (Target: ad; Generality: 50%)

Experiment  Prediction Experiment 
Evaluating on classifier runExperiment(train, test = test, classifier = make_Weka_classifier("weka/classifiers/lazy/IBk"), target = "ad", controlpars = alist(K = 1)) 
=========================================================================

Evaluating on classifier classifier - threshold: 0 
=======================================
Split: 0%  -- Test set given separately

TRUE                     FALSE
-------------------      ----------------------
Accuracy:   0.583         Accuracy:  0.625
Precision:  0.609         Precision: 0.600      
Recall:     0.583         Recall:    0.625      
F_1:        0.596         F_1:       0.612
Overall accuracy: 0.604
data frame passed as argument (Target: ad; Generality: 50%)

Experiment  Prediction Experiment 
Evaluating on classifier runExperiment(train, test = test, classifier = make_Weka_classifier("weka/classifiers/functions/LibLINEAR"), target = "ad", controlpars = alist(S = 5, C = 0.1, W = 2)) 
=========================================================================

Evaluating on classifier classifier - threshold: 0 
=======================================
Split: 0%  -- Test set given separately

TRUE                     FALSE
-------------------      ----------------------
Accuracy:   0.542         Accuracy:  0.792
Precision:  0.722         Precision: 0.633      
Recall:     0.542         Recall:    0.792      
F_1:        0.619         F_1:       0.704
Overall accuracy: 0.667
data frame passed as argument (Target: ad; Generality: 50%)

Experiment  Prediction Experiment 
Evaluating on classifier runExperiment(train, test = test, classifier = make_Weka_classifier("weka/classifiers/trees/RandomForest"), target = "ad") 
=========================================================================

Evaluating on classifier classifier - threshold: 0 
=======================================
Split: 0%  -- Test set given separately

TRUE                     FALSE
-------------------      ----------------------
Accuracy:   0.667         Accuracy:  0.500
Precision:  0.571         Precision: 0.600      
Recall:     0.667         Recall:    0.500      
F_1:        0.615         F_1:       0.545
Overall accuracy: 0.583
```

#### Summary of results for CV

ldaacc_loso | dtacc_loso | knnacc_loso |**svmacc_loso**| rfacc_loso |  mean 
------------|------------|-------------|---------------|------------|-------
0.556       | 0.444      | 0.426       | **0.593**     | 0.565      |  0.517


#### Summary of results on test set

ldaacc | dtacc | knnacc |**svmacc**| rfacc |  mean 
-------|-------|--------|----------|-------|-------
0.604  | 0.562 | 0.604  |**0.667** | 0.583 |  0.604 


Best results on test set for SVM:

```r
printResultsSummary(rs2$svm,1)

Summary for experiment "Prediction Experiment", Avg over 1-fold evaluation for 
runExperiment(train, test = test, classifier = make_Weka_classifier("weka/classifiers/functions/LibLINEAR"), target = "ad", controlpars = alist(S = 5, C = 0.1, W = 2))
on classifier classifier and threshold 0.
=====================================

TRUE                     FALSE
-------------------      ----------------------
Accuracy:   0.542         Accuracy:  0.792
Precision:  0.722         Precision: 0.633 
Recall:     0.542         Recall:    0.792 
F_1:        0.619         F_1:       0.704
Precision:  0.722         Precision: 0.633       (macro-averaged)
Recall:     0.542         Recall:    0.792       (macro-averaged)
F_1:        0.619         F_1:       0.704       (macro-averaged)
Overall accuracy: 0.667
```

This is similar to the performance I got in my CBMS'17 paper (Luz,
2017), with a similar set of features.


NB: For convenience, here is how we generate a LaTeX table rows for the
results above:

```r
    batchSummaryLaTeXTableRow(batchSummary(rs2))
```

```latex
ldaacc_loso & dtacc_loso & knnacc_loso & svmacc_loso & rfacc_loso &  mean \\
0.519 & 0.667 & 0.426 & 0.565 & 0.583 &  0.552 \\
ldaacc & dtacc & knnacc & svmacc & rfacc &  mean \\
0.604 & 0.562 & 0.604 & 0.667 & 0.583 &  0.604 \\
```

## Regression results

Run 

```r
train <- getADReSsubSet(a2$train, features=2, target='mmse')
test <- getADReSsubSet(a2$test, features=2, target='mmse')
 r <- runADReSSregressionBatch(train, test)

[output omitted]

batchSummaryLaTeXTableRow(batchSummary(r))
 linearcv      dtcv      gpcv     svmcv lsboostcv 
 7.391096  7.603126  7.189247  8.013289  7.146140 
  linear       dt       gp      svm  lsboost 
6.291143 6.843840 6.579347 6.189831 7.712632 
```

#### Summary of results for CV (RMSE)

linearcv    | dtcv       | gpcv        | svmcv         | lsboostcv  |  mean 
------------|------------|-------------|---------------|------------|-------
7.391       | 7.603      | 7.189       | 8.013         | 7.146      |  7.469 


#### Summary of results on test set (RMSE)

linear | dt    | gp     | svm      | lsboost |  mean 
-------|-------|--------|----------|---------|-------
6.291  | 6.844 | 6.579  | 6.19     | 7.713   |  6.723 
